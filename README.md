This repository contains implementations of some algorithms from the 2018-2019 version of the Part II Bioinformatics
course at Cambridge. The implementations are written in Python/NumPy and are optimised for clarity and similarity to the
lectures rather than efficiency. Currently, the following algorithms have been implemented:

- Hamming distance calculation
- Longest common subsequence
- Local and global alignment
- The Nussinov algorithm

There are also a few helper functions to represent biological knowledge (base pairs) and to nicely display the output of the algorithms.

The contents of this repository are under a slightly modified version of the MIT license.
