"""
Implementations of various alignment algorithms from the 2018-2019 vesion of the Part II Bioinformatics course at Cambridge.

Algorithms covered are Hamming distance calculation, longest common subsequence, local and global aligment computation,
and the Nussinov algorithm.
"""
import numpy as np

# constants for backtracking in LCS
DIAG, UP, LEFT = 0, 1, 2


def hamming_distance(first, second):
    """
    Calculate the Hamming distance between two strings.
    """
    if len(first) != len(second):
        raise ValueError("Hamming distance undefined for strings of different length")

    total = 0
    for x, y in zip(first, second):
        if x != y:
            total += 1

    return total


def longest_common_subsequence(first, second):
    """
    Calculate the longest common subsequence of two strings using dynamic programming.

    Returns the length of the LCS and a table that can be used to reconstruct it.
    """
    n, m = len(first), len(second)

    # S is the table used to store the LCS length at a given point
    S = np.empty((n, m), dtype=int)

    # B is the table used to store the previous element in a candidate LCS
    B = np.empty((n, m), dtype=int)

    for i in range(n):
        S[i, 0] = 0

    for j in range(m):
        S[0, j] = 0

    for i in range(1, n):
        for j in range(1, m):
            length, direction = max((S[i - 1, j], UP),
                                    (S[i, j - 1], LEFT),
                                    (S[i - 1, j - 1] + 1 if first[i] == second[j] else -1, DIAG),
                                    key=lambda x: x[0])
            S[i, j] = length
            B[i, j] = direction

    return S[n -1, m - 1], B


def LCS_table_to_string(first, second, B):
    """
    Traceback through the table from `longest_common_subsequence` and convert the LCS in it to a string.

    The argument `second` isn't used but is included for consistency with `alignment_to_string`.
    """
    output = []
    def _traceback(i, j):
        if i == 0 or j == 0:
            return

        elif B[i, j] == DIAG:
            _traceback(i - 1, j - 1)
            output.append(first[i])

        elif B[i, j] == UP:
            _traceback(i - 1, j)

        else:
            _traceback(i, j - 1)

    _traceback(len(first) - 1, len(second) - 1)
    return "".join(output)



def alignment_to_string(first, second, indices):
    """
    Display an alignment, with "-" for gaps.
    """
    if len(indices) == 0:
        return ""

    first_line = []
    second_line = []
    for sequence, output_line, x in ((first, first_line, 0), (second, second_line, 1)):
        output_line.append(sequence[indices[0][x]])

        for n, (i, j) in enumerate(indices[1:]):
            index = (i, j)[x]
            char = sequence[index]
            if index == indices[n][x]:        # n not n-1 because we're enumerating over indices[1:]
                char = "-"

            output_line.append(char)

    return "\n".join(("".join(first_line), "".join(second_line)))


def local_initialisation(first, second, F, previous):
    """
    Given two strings to locally align, return the initialized score and previous table for the alignment algorithm.
    """
    for i in range(len(first)):
        F[i, 0] = 0
        previous[i, 0] = None

    for j in range(len(second)):
        F[0, j] = 0
        previous[0, j] = None

    return F, previous


def global_initialisation(first, second, F, previous):
    """
    Given two strings to globally align, return the initialized score and previous table for the alignment algorithm.
    """
    for i in range(len(first)):
        F[i, 0] = -i*(1)
        previous[i, 0] = (i - 1, 0)

    for j in range(len(second)):
        F[0, j] = -j*(1)
        previous[0, j] = (j - 1, 0)

    previous[0, 0] = None

    return F, previous


def local_iterate(s, indel_penalty):
    """
    Returns a score function for local alignment to pass to `align`.

    The function `s` takes two characters that are being aligned and scores them (for instances gives +1 if they are the
    same and -1 if they are different). The value `ìndel_penalty` is the amount the score is reduced for each mismatch.
    """
    def local_alignment_iteration(first, second, i, j, F):
        """
        Return a score and previous (i, j) pair for local alignment between strings `first` and `second` with score table
        `F` at cell `(i, j)`.

        The placement of (0, None) as the first element in the max means shorter alignments are favoured (all else being
        equal).
        """
        return max([(0, None),
                   (F[i - 1, j] - indel_penalty, (i - 1, j)),
                   (F[i, j - 1] - indel_penalty, (i, j - 1)),
                   (F[i - 1, j - 1] + s(first[i], second[j]), (i - 1, j - 1))],
                   key=lambda x: x[0])

    return local_alignment_iteration


def global_iterate(s, indel_penalty):
    """
    Returns a score function for global alignment to pass to `align`. The function `s` takes two characters that are
    being aligned and scores them (for instance gives +1 if they are the same and -1 if they are different). The value
    `ìndel_penalty` is the amount the score is reduced for each mismatch.
    """
    def global_alignment_iteration(first, second, i, j, F):
        """
        Return a score and previous (i, j) pair for global alignment between strings `first` and `second` with score table
        `F` at cell `(i, j)`.
        """
        return max([(F[i - 1, j] - indel_penalty, (i - 1, j)),
                    (F[i, j - 1] - indel_penalty, (i, j - 1)),
                    (F[i - 1, j - 1] + s(first[i], second[j]), (i - 1, j - 1))],
                   key=lambda x: x[0])

    return global_alignment_iteration


def local_endpoint(first, second, F):
    """
    Get the end indices for a local alignment from the score table.
    """
    return next(zip(*np.where(F == F.max())))


def global_endpoint(first, second, F):
    """
    Get the end indices for a global alignment.
    """
    return (len(first) - 1, len(second) - 1)


def align(first, second, score_function, initialisation, endpoint_function):
    """
    Calculate the local alignment between two strings, with scoring matrix of +1 for matches and -1 for indels and
    mismatches.

    Return a list of indices into `first` and `second`, and the score
    """
    n = len(first)
    m = len(second)
    F = np.empty((n, m), dtype=int)
    previous = np.empty((n, m), dtype=object)

    F, previous = initialisation(first, second, F, previous)

    for i in range(1, n):
        for j in range(1, m):
            value, indices = score_function(first, second, i, j, F)
            F[i, j] = value
            previous[i, j] = indices

    best = endpoint_function(first, second, F)

    indices = []
    current = best
    while previous[current] is not None:
        indices.append(current)
        current = previous[current]

    return indices[::-1], F[best]


def rna_pair(rna, i, j):
    """
    Return 1 if the characters at positions `i` and `j` of a string of RNA are a complementary base pair, 0 otherwise.

    RNA complementary bases are A-U, G-U and G-C.
    """
    vals = rna[i], rna[j]
    if vals in (("A", "U"), ("U", "A"), ("G", "C"), ("C", "G"), ("G", "U"), ("U", "G")):
        return 1
    else:
        return 0


def nussinov(rna):
    """
    Perform the Nussinov algorithm on a string of RNA, returning the filled table.
    """
    n = len(rna)
    table = np.empty((n, n), dtype=int)

    for i in range(n):
        table[i, i - 1] = 0
        table[i, i] = 0

    for index_del in range(1, n):
        for i in range(n - index_del):
            j = i + index_del
            pairs = [table[i, k] + table[k + 1, j] for k in range(i + 1, j)]
            if pairs:
                inner_max = max(pairs)
            else:
                inner_max = 0

            score = max(table[i + 1, j], table[i, j - 1], table[i + 1, j - 1] + rna_pair(rna, i, j), inner_max)
            table[i, j] = score

    return table


def nussinov_traceback(rna, table):
    """
    Perform traceback on the table produced by the Nussinov algorithm, returning the indices of paired bases.

    This will produce only one out of many valid tracebacks: the different cases can be arbitrarily reprioritised (by
    reordering the `if` statements), even within a run of the algorithm.
    """
    n = len(rna)
    output = []
    stack = [(0, n - 1)]
    while stack:
        i, j = stack.pop()
        if i >= j:
            continue

        elif table[i + 1, j] == table[i, j]:
            stack.append((i + 1, j))

        elif table[i, j - 1] == table[i, j]:
            stack.append((i, j - 1))

        elif table[i + 1, j - 1] + rna_pair(rna, i, j) == table[i, j]:
            output.append((i, j))
            stack.append((i + 1, j - 1))

        else:
            for k in range(i + 1, j):
                if table[i, k] + table[k + 1, j] == table[i, j]:
                    stack.append((k + 1, j))
                    stack.append((i, k))
                    break

    return output


def format_nussinov_table(rna, table):
    """
    Format the output of the Nussinov algorithm as a string.
    """
    lines = []
    n = len(rna)
    lines.append(" ".join(" " + rna))

    for i in range(n):
        line = []
        line.append(rna[i])
        for j in range(n):
            if j >= i - 1:
                line.append(str(table[i, j]))
            else:
                line.append(" ")
        lines.append(" ".join(line))

    return "\n".join(lines)


if __name__ == "__main__":
    v, w = "ATATATAT", "TATATATA"
    print("Hamming distance between {} and {} is {}".format(v, w, hamming_distance(v, w)))
    print()

    first, second = "TAATGTTCA", "ATCGTCCA"
    length, b = longest_common_subsequence(first, second)
    print("Longest common subsequence of {} and {} is {} with length {}".format(first, second,
                                                                                LCS_table_to_string(first, second, b),
                                                                                length))
    print()

    indices, score = align(first, second, local_iterate(lambda x, y: 1 if x == y else -1, 1), local_initialisation, local_endpoint)
    print("Local alignment of {} and {} with indel and mismatch cost -1:".format(first, second))
    print(alignment_to_string(first, second, indices))
    print("Score is {}".format(score))
    print()

    indices, score = align(first, second, global_iterate(lambda x, y: 1 if x == y else -2, 1), global_initialisation, global_endpoint)
    print("Global alignment of {} and {} with indel cost -1 and mismatch cost -2:".format(first, second))
    print(alignment_to_string(first, second, indices))
    print("Score is {}".format(score))
    print()

    rna = "GGGAAAUCC"
    table = nussinov(rna)
    print("Nussinov algorithm applied to {}:".format(rna))
    print(format_nussinov_table(rna, table))
    print("Base pairs:")
    print(nussinov_traceback(rna, table))
